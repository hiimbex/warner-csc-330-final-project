# bexobot ⓒ 2018

[![](https://img.shields.io/twitter/follow/hiimbexo.svg?style=social&logo=twitter&label=Follow)](https://twitter.com/hiimbexo)

The goal of this final project is to build a line following robot, using an ATMEGA328PB, coding it in AVR Assembly.

## Hardware Setup

![Schematic](images/schematic.JPG) 

This project will use the [ATmega328PB](https://www.microchip.com/wwwproducts/en/ATmega328PB), an 8 bit AVR microcontroller, controlling an [RGB color sensor](https://www.adafruit.com/product/1334) as well as a pair of motors.

## Implementation details

1. bexobot will read input from the RGBC functionality of the color sensor and determine whether that color is believed to be black or white.
1. If bexobot detects black the robot with stay the course with forward power to both motors
1. Alternatively if bexobot detects white, bexobot will power one motor forward and the other motor backwards in order to turn
1. bexobot will then repeat these previous steps, in as quick succession in possible, in order to react to changes in the course as quickly as possible.

## Potential Obstacles

1. Sharp turns
1. Gear grinding
1. Not being able to make adjustments quickly enough
1. Making too drastic of a correction
