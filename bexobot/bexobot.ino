#include <SparkFun_TB6612.h>
#include <Wire.h>
#include <Adafruit_TCS34725.h>
#include <elapsedMillis.h>
/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);
// Motor A
int AIN1 = 4; //Direction
int AIN2 = 5; //Direction
int PWMA = A6;
// Motor B
int BIN1 = 2; //Direction
int BIN2 = 3; //Direction
int PWMB = A7;

void getRawData_noDelay(uint16_t *r, uint16_t *g, uint16_t *b, uint16_t *c)
{
  *c = tcs.read16(TCS34725_CDATAL);
  *r = tcs.read16(TCS34725_RDATAL);
  *g = tcs.read16(TCS34725_GDATAL);
  *b = tcs.read16(TCS34725_BDATAL);
}

void setup() {
  Serial.begin(9600);
  if (tcs.begin()) {
    Serial.println("Found sensorz");
    Serial.println("hello world");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }
  // Initialize motor pins
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(PWMB, OUTPUT);
}

void loop() {
  Serial.println("LOOP-DEE-DOO");
  bool color = checkColor();
  Serial.println(color);
  if (!color) {
    Serial.println("we are off course!");
    sweepCourse();
    //onCourse();
  } else {
    Serial.println("we are on course!");
    onCourse();
  }
}

void onCourse() {
  move(1, 0, 70); // motor 1 full speed
  move(2, 0, 70); // motor 2 full speed
}

void sweepCourse() {
  elapsedMillis timeSpent;
  bool foundLineRight = false;
  bool found = false;
  // check right
  while (timeSpent < 3000 && !found) {
    bool x = checkColor();
    Serial.println(x);
    move(1, 1, 50); // motor 1 full speed right
    move(2, 0, 50); // motor 2 full speed left
    if (checkColor()) {
      //move(1, 0, 30);
      timeSpent = 0;
      // adjust back for motor slow reaction/overcompensation
      while (timeSpent < 300) {
        move(1, 0, 50); // motor 1 full speed left
        move(2, 1, 50);
      }
      Serial.println("Found the course!");
      found = true;
      return;
    }
  }
  Serial.println(timeSpent);
  if (!foundLineRight) {
    while (!found) {
      move(1, 0, 50); // motor 1 full speed left
      move(2, 1, 50); // motor 2 full speed right
      if (checkColor()) {
        Serial.println("Found the course!");
        timeSpent = 0;
        // adjust back for motor slow reaction/overcompensation
        while (timeSpent < 300) {
          move(1, 1, 50); // motor 1 full speed left
          move(2, 0, 50);
        }
        found = true;
        return;
      }
    }
  }
  return;
}

void move(int motor, int direction, int speed) {
  //Move specific motor
  //motor: 0 for B 1 for A
  //direction: 0 clockwise, 1 counter-clockwise
  boolean inPin1 = LOW;
  boolean inPin2 = HIGH;
  if (direction == 1) {
    inPin1 = HIGH;
    inPin2 = LOW;
  }

  if (motor == 1) {
    digitalWrite(AIN1, inPin1);
    digitalWrite(AIN2, inPin2);
    analogWrite(PWMA, speed);
  } else {
    digitalWrite(BIN1, inPin1);
    digitalWrite(BIN2, inPin2);
    analogWrite(PWMB, speed);
  }
}

bool checkColor() {
  uint16_t r, g, b, c;
  tcs.getRawData(&r, &g, &b, &c);
  //Serial.println(AIN2);
  if ((float)r > 500 && (float)r > 500 && (float)r > 500) {
    // white
    Serial.println("white");
    return false;
  } else {
    Serial.println("black");
    return true;
  }
}

